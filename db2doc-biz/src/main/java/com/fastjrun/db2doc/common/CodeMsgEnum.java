/*
 * Copyright (C) 2021 Fastjrun, Inc. All Rights Reserved.
 */
package com.fastjrun.db2doc.common;

import com.fastjrun.apiworld.common.CodeMsgI;

public enum CodeMsgEnum implements CodeMsgI {
  CodeMsg_2000(CODE_OK, CODE_OK_MSG),

  CodeMsg_21001(21001, "dbconnnetion is not valid"),

  CodeMsg_21002(21002, "export file is not ok"),

  CodeMsg_21003(21003, "download file is not valid"),

  CodeMsg_59998(59998, "其他异常"),

  CodeMsg_59999(59999, "系统错误");

  private Integer code;
  private String msg;

  CodeMsgEnum(Integer code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  @Override
  public Integer getCode() {
    return this.code;
  }

  @Override
  public String getMsg() {
    return this.msg;
  }
}
