package com.fastjrun.db2doc.dao;

import com.fastjrun.db2doc.mapper.DbConnectionMapper;

public interface DBConnectionDao extends DbConnectionMapper {
}
