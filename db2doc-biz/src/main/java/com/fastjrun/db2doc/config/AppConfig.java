package com.fastjrun.db2doc.config;

import cn.smallbun.screw.core.process.ProcessConfig;
import lombok.Data;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author cuiyingfeng
 */
@Configuration
@ComponentScan("com.fastjrun.db2doc")
@MapperScan({"com.fastjrun.db2doc.dao"})
@Component
@ConfigurationProperties(prefix = "fastjrun.db2doc")
@Data
public class AppConfig {
  // 生成文件所在的目录
  String dataDir;

  String version;

  String description;
  String[] ignoreTableName;
  String[] ignorePrefix;
  String[] ignoreSuffix;

  @Bean
  AppBean getAppBean() {
    AppBean appBean = new AppBean();
    appBean.setProcessConfig(getProcessConfig());
    appBean.setDataDir(dataDir);
    appBean.setVersion(version);
    appBean.setDescription(description);
    return appBean;
  }

  ProcessConfig getProcessConfig() {

    return ProcessConfig.builder()
        // 根据名称指定表生成
        .designatedTableName(new ArrayList<>())
        // 根据表前缀生成
        .designatedTablePrefix(new ArrayList<>())
        // 根据表后缀生成
        .designatedTableSuffix(new ArrayList<>())
        // 忽略表名
        .ignoreTableName(Arrays.asList(ignoreTableName))
        // 忽略表前缀
        .ignoreTablePrefix(Arrays.asList(ignorePrefix))
        // 忽略表后缀
        .ignoreTableSuffix(Arrays.asList(ignoreSuffix))
        .build();
  }
}
