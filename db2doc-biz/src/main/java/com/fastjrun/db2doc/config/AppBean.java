package com.fastjrun.db2doc.config;

import cn.smallbun.screw.core.process.ProcessConfig;
import lombok.Data;

@Data
public class AppBean {
    // 生成文件所在的目录
    String dataDir;

    String version;

    String description;

    private ProcessConfig processConfig;

}
