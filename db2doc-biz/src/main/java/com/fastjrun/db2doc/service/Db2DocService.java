package com.fastjrun.db2doc.service;

import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.process.ProcessConfig;

import javax.sql.DataSource;

/**
 * @author cuiyingfeng
 */
public interface Db2DocService {
  /**
   * @param dataSource
   * @param type
   * @param
   */
  String generatorDoc(
      DataSource dataSource, EngineFileType type);
}
