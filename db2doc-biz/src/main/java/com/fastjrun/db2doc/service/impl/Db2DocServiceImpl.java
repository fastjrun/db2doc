package com.fastjrun.db2doc.service.impl;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecuteExt;
import com.fastjrun.db2doc.config.AppBean;
import com.fastjrun.db2doc.service.Db2DocService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * @author cuiyingfeng
 */
@Service("db2DocService")
public class Db2DocServiceImpl implements Db2DocService {

  @Resource AppBean appBean;

  @Override
  public String generatorDoc(DataSource dataSource, EngineFileType type) {

    EngineConfig engineConfig =
        EngineConfig.builder()
            // 生成文件路径，自己mac本地的地址，这里需要自己更换下路径
            .fileOutputDir(appBean.getDataDir())
            // 打开目录
            .openOutputDir(false)
            // 文件类型
            .fileType(type)
            // 生成模板实现
            .produceType(EngineTemplateType.freemarker)
            .build();

    // 生成文档配置（包含以下自定义版本号、描述等配置连接）
    Configuration config =
        Configuration.builder()
            .version(appBean.getVersion())
            .description(appBean.getDescription())
            .dataSource(dataSource)
            .engineConfig(engineConfig)
            .produceConfig(appBean.getProcessConfig())
            .build();
    DocumentationExecuteExt documentationExecute = new DocumentationExecuteExt(config);
    // 执行生成
    documentationExecute.execute();

    return documentationExecute.getDocName();
  }
}
