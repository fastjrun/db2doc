package com.fastjrun.db2doc.helper;

import com.fastjrun.db2doc.entity.DbConnection;
import com.fastjrun.db2doc.vo.DBConnectionVO;

public class EntityToVOConverter {

  public static DBConnectionVO convert(DbConnection dbConnection) {
    DBConnectionVO dbConnectionVO = new DBConnectionVO();
    dbConnectionVO.setId(dbConnection.getId());
    dbConnectionVO.setName(dbConnection.getName());
    dbConnectionVO.setDriver(dbConnection.getDriver());
    dbConnectionVO.setUrl(dbConnection.getUrl());
    dbConnectionVO.setUsername(dbConnection.getUsername());
    dbConnectionVO.setPassword(dbConnection.getPassword());
    return dbConnectionVO;
  }
}
