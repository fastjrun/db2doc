package com.fastjrun.db2doc.helper;

import com.fastjrun.db2doc.dto.DBConnectionDTO;
import com.fastjrun.db2doc.entity.DbConnection;

public class DTOToEntityConverter {

  public static DbConnection convert(DBConnectionDTO dbConnectionDTO) {
    DbConnection dbConnection = new DbConnection();
    dbConnection.setId(dbConnectionDTO.getId());
    dbConnection.setName(dbConnectionDTO.getName());
    dbConnection.setDriver(dbConnectionDTO.getDriver());
    dbConnection.setUrl(dbConnectionDTO.getUrl());
    dbConnection.setUsername(dbConnectionDTO.getUsername());
    dbConnection.setPassword(dbConnectionDTO.getPassword());
    return dbConnection;
  }
}
