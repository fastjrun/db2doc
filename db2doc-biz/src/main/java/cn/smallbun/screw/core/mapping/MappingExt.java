package cn.smallbun.screw.core.mapping;

import cn.smallbun.screw.core.exception.MappingException;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;

public class MappingExt {
    /**
     * 根据列标签获取列信息
     *
     * @param resultSet {@link ResultSet} 对象
     * @param clazz     领域类型
     * @param <T>       领域泛型
     * @return 领域对象
     * @throws MappingException MappingException
     */
    public static <T> List<T> convertListByColumnLabel(ResultSet resultSet,
                                                        Class<T> clazz) throws MappingException {
        //存放列名和结果
        List<Map<String, Object>> values = new ArrayList<>(16);
        //结果集合
        List<T> list = new ArrayList<>();
        try {
            //处理 ResultSet
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            //迭代
            while (resultSet.next()) {
                //map object
                HashMap<String, Object> value = new HashMap<>(16);
                //循环所有的列，获取列名，根据列名获取值
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = metaData.getColumnLabel(i);
                    value.put(columnName, resultSet.getString(i));
                }
                //add object
                values.add(value);
            }
            //获取类数据
            List<FieldMethod> fieldMethods = getFieldMethods(clazz);
            //循环集合，根据类型反射构建对象
            for (Map<String, Object> map : values) {
                T rsp = getObject(clazz, fieldMethods, map);
                list.add(rsp);
            }
        } catch (Exception e) {
            throw new MappingException(e);
        }
        return list;
    }

    /**
     * 获取对象
     *
     * @param clazz        class
     * @param fieldMethods List<FieldMethod>
     * @param map          数据集合
     * @param <T>          领域泛型
     * @return 领域对象
     * @throws InstantiationException    InstantiationException
     * @throws IllegalAccessException    IllegalAccessException
     * @throws InvocationTargetException InvocationTargetException
     */
    private static <T> T getObject(Class<T> clazz, List<FieldMethod> fieldMethods,
                                   Map<String, Object> map) throws InstantiationException,
            IllegalAccessException,
            InvocationTargetException {
        T rsp = clazz.newInstance();
        //设置属性值
        for (FieldMethod filed : fieldMethods) {
            Field field = filed.getField();
            Method method = filed.getMethod();
            MappingField jsonField = field.getAnnotation(MappingField.class);
            if (!Objects.isNull(jsonField)) {
                method.invoke(rsp, map.get(jsonField.value()));
            }
        }
        return rsp;
    }

    /**
     * 根据类型获取 FieldMethod
     *
     * @param clazz {@link Class}
     * @param <T>   {@link T}
     * @return {@link List<FieldMethod>}
     * @throws IntrospectionException IntrospectionException
     * @throws NoSuchFieldException   NoSuchFieldException
     */
    private static <T> List<FieldMethod> getFieldMethods(Class<T> clazz) throws IntrospectionException,
            NoSuchFieldException {
        //结果集合
        List<FieldMethod> fieldMethods = new ArrayList<>();
        //BeanInfo
        BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        //循环处理值
        for (PropertyDescriptor pd : pds) {
            Method writeMethod = pd.getWriteMethod();
            if (writeMethod == null) {
                continue;
            }
            //获取字段
            Field field = clazz.getDeclaredField(pd.getName());
            //获取只写方法
            FieldMethod fieldMethod = new FieldMethod();
            fieldMethod.setField(field);
            fieldMethod.setMethod(writeMethod);
            //放入集合
            fieldMethods.add(fieldMethod);
        }
        return fieldMethods;
    }
}
