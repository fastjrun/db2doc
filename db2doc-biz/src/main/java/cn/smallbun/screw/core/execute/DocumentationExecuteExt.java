package cn.smallbun.screw.core.execute;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineFactory;
import cn.smallbun.screw.core.engine.TemplateEngine;
import cn.smallbun.screw.core.exception.BuilderException;
import cn.smallbun.screw.core.metadata.model.DataModel;
import cn.smallbun.screw.core.process.DataModelProcess;
import cn.smallbun.screw.core.util.ExceptionUtils;

/**
 * @author cuiyingfeng
 */
public class DocumentationExecuteExt extends AbstractExecute {


    /**
     * 生成文件文成
     */
    private String docName;

    public DocumentationExecuteExt(Configuration config) {
        super(config);
    }



    /**
     * 执行
     *
     * @throws BuilderException BuilderException
     */
    @Override
    public void execute() throws BuilderException {
        try {
            long start = System.currentTimeMillis();
            //处理数据
            DataModel dataModel = new DataModelProcess(config).process();
            this.docName=getDocName(dataModel.getDatabase());
            //产生文档
            TemplateEngine produce = new EngineFactory(config.getEngineConfig()).newInstance();
            produce.produce(dataModel,this.docName );
            logger.debug("database document generation complete time consuming:{}ms",
                    System.currentTimeMillis() - start);
        } catch (Exception e) {
            throw ExceptionUtils.mpe(e);
        }
    }

    public String getDocName(){
        return this.docName;
    }
}