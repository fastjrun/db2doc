package cn.smallbun.screw.core.query.h2.model;

import cn.smallbun.screw.core.metadata.Database;
import lombok.Data;

/**
 * @author cuiyingfeng
 */
@Data
public class H2DatabaseModel implements Database {


    private static final long serialVersionUID = -1045200056716008133L;
    /**
     * 数据库名称
     */
    private String            database;


}
