package cn.smallbun.screw.core.query.h2.model;

import cn.smallbun.screw.core.mapping.MappingField;
import cn.smallbun.screw.core.metadata.PrimaryKey;
import lombok.Data;

/**
 * @author cuiyingfeng
 */
@Data
public class H2PrimaryKeyModel implements PrimaryKey {


    private static final long serialVersionUID = 4990963904884629764L;
    /**
     * tableCat
     */
    @MappingField(value = "TABLE_CAT")
    private String            tableCat;
    /**
     * 表名
     */
    @MappingField(value = "TABLE_NAME")
    private String            tableName;
    /**
     * 主键名称
     */
    @MappingField(value = "PK_NAME")
    private String            pkName;
    /**
     *
     */
    @MappingField(value = "TABLE_SCHEM")
    private String            tableSchem;
    /**
     * 列名
     */
    @MappingField(value = "COLUMN_NAME")
    private String            columnName;
    /**
     *
     */
    @MappingField(value = "KEY_SEQ")
    private String            keySeq;
}