package cn.smallbun.screw.core.query.h2.model;

import cn.smallbun.screw.core.mapping.MappingField;
import cn.smallbun.screw.core.metadata.Table;
import lombok.Data;

/**
 * @author cuiyingfeng
 */
@Data
public class H2TableModel implements Table {

    private static final long serialVersionUID = 2210422015364015966L;
    /**
     * tableCat
     */
    @MappingField(value = "TABLE_CAT")
    private String            tableCat;
    /**
     * 表名
     */
    @MappingField(value = "TABLE_NAME")
    private String            tableName;
    /**
     *
     */
    @MappingField(value = "SELF_REFERENCING_COL_NAME")
    private String            selfReferencingColName;
    /**
     *
     */
    @MappingField(value = "TABLE_CAT")
    private String            tableSchem;
    /**
     *
     */
    @MappingField(value = "TYPE_SCHEM")
    private String            typeSchem;
    /**
     *
     */
    @MappingField(value = "TABLE_CAT")
    private Object            typeCat;
    /**
     * 表类型
     */
    @MappingField(value = "TABLE_TYPE")
    private String            tableType;
    /**
     * 备注
     */
    @MappingField(value = "REMARKS")
    private String            remarks;
    /**
     *
     */
    @MappingField(value = "REF_GENERATION")
    private String            refGeneration;
    /**
     * 类型名称
     */
    @MappingField(value = "TYPE_NAME")
    private String            typeName;
}
