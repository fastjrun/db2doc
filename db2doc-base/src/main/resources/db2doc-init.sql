insert into db_connection (`name`, `driver`, `url`, `username`, `password`)
VALUES ('mysql', 'com.mysql.cj.jdbc.Driver',
        'jdbc:mysql://10.168.1.240:3306/db2doc?serverTimezone=GMT&useUnicode=true&characterEncoding=UTF-8&useSSL=false&allowPublicKeyRetrieval=true',
        'root', 'root'),
       ('oracle', 'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@10.168.1.240:1521:helowin', 'system', 'system'),
       ('db2', 'com.ibm.db2.jcc.DB2Driver', 'jdbc:db2://10.168.1.240:50000/db2doc:currentSchema=TEST;', 'db2inst1', 'db2inst1-pwd');