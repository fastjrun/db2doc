-- ----------------------------
-- Table structure for db_connection
-- ----------------------------
DROP TABLE IF EXISTS `db_connection`;
CREATE TABLE `db_connection`
(
    `id`       bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`     varchar(64)  NOT NULL DEFAULT '' COMMENT '名称',
    `driver`   varchar(100) NOT NULL DEFAULT '' COMMENT 'h2;mysql;db2;oracle',
    `url`      varchar(255) NOT NULL DEFAULT '' COMMENT 'url',
    `username` varchar(64)  NOT NULL DEFAULT '' COMMENT '用户名',
    `password` varchar(64)  NOT NULL DEFAULT '' COMMENT '密码',
    PRIMARY KEY (`id`)
) COMMENT ='数据库连接表';