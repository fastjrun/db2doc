#!/bin/bash


FILE=~/db2doc-h2.mv.db
if test -f "$FILE"; then
    INIT="never"
fi

java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar db2doc.jar --spring.datasource.initialization-mode=${INIT}
