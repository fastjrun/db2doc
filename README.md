# 数据库文档工具

使用本工具可以实时连接数据库快速生成 markdown、html和word格式的数据库文档
- 依赖h2
- 基于spring-boot2.1.3
- 基于https://gitee.com/leshalv/screw 开发，理论上支持MySQL/MariaDB/SqlServer/Oracle/PostgreSQL/TIDB/CacheDB等
- 通过浏览器维护
- 封装Docker，同时支持arm和x86架构

### 使用方法

### 下载源码并编译
```
git clone https://gitee.com/fastjrun/db2doc.git
cd db2doc
sh build.sh package_server
```
output目录下生成db2doc.jar

### 部署

#### 原生部署
将db2doc.jar和db2doc.sh部署到同一目录下，比如/opt/db2doc
```
cd /opt/db2doc
# 采用后台部署方式  
INIT=always nohup sh db2doc.sh &
```

#### 容器化部署
也可以直接通过docker镜像pi4k8s/db2doc:2.0进行部署，这个镜像不仅可以在一般x86服务器使用，也可以在树莓派4B上直接使用
```
docker run -itd --name db2doc -p 8080:8080 \
-e JAVA_OPTS="-Xms1g -Xmx1g" pi4k8s/db2doc:2.0
```

### 使用

- 数据库连接记录维护

http:{ip}:8080

![输入图片说明](static/images/db2doc.png)

通过本配置页可添加多条数据库连接记录。
- 测试连接

维护好记录后可点击操作列的"测试连接"按钮测试配置连接是否OK。
- 导出数据库文档
  
支持导出markdown和word文档直接下载，导出html格式文档可直接在浏览器浏览。

