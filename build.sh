#!/bin/bash

echo "build ..."
rm -rf output/*
mkdir -p output

if [ "ci" = $1 ] ; then
    mvn clean compile -pl db2doc-base,db2doc-bundle -am -Dbdgc.skip=false
elif [ "package_server" = $1 ] ; then
    mvn clean package -pl db2doc-server -am -Dbdgc.skip=false
    cp db2doc-server/target/db2doc.jar ./output
    cp Dockerfile ./output
    cp db2doc.sh ./output
elif [ "package_mock_server" = $1 ] ; then
    mvn clean package -pl db2doc-mock-server -am -Dbdmgc.skip=false
    cp db2doc-mock-server/target/db2doc-mock-server.jar ./output
elif [ "clean_all" = $1 ] ; then
    mvn clean
    rm -rf output
    rm -rf db2doc-bundle/src
    rm -rf db2doc-bundle-mock/src
fi
echo "build done."
