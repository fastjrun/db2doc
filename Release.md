### V2.0
- 使用[sdkg](https://gitee.com/fastjrun/sdkg)重构，增加页面和数据库（h2）支撑
- 支持维护多条数据库连接记录
- 支持mysql、db2和oracle，理论上还支持mariadb/tidb/postgresql/sqlserver等
- 支持导出数据库文档，支持word、markdown和html
- 封装Docker，同时支持arm和x86架构

### V1.0
- 依赖h2
- 基于spring-boot2.1.3
- 基于https://gitee.com/leshalv/screw
- 封装Docker，同时支持arm和x86架构
