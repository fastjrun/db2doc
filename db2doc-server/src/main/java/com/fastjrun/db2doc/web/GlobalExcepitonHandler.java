/*
 * Copyright (C) 2021 Fastjrun, Inc. All Rights Reserved.
 */
package com.fastjrun.db2doc.web;

import com.fastjrun.apiworld.helper.ResultHelper;
import com.fastjrun.apiworld.service.common.ServiceException;
import com.fastjrun.apiworld.vo.ResultModel;
import com.fastjrun.db2doc.common.CodeMsgEnum;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExcepitonHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  @ResponseBody
  public Object expHandler(Exception e) {
    ResultModel resultModel;
    if (e instanceof ServiceException) {
      resultModel =
          ResultHelper.fail(((ServiceException) e).getCode(), ((ServiceException) e).getMsg());
    } else if(!"".equals(e.getMessage())){
      resultModel = ResultHelper.fail(CodeMsgEnum.CodeMsg_59998.getCode(),e.getMessage());
    }else{

      resultModel = ResultHelper.fail(CodeMsgEnum.CodeMsg_59999);
    }
    return resultModel;
  }
}
