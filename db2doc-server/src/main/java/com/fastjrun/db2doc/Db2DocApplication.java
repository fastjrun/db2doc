package com.fastjrun.db2doc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Db2DocApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext ac = SpringApplication.run(Db2DocApplication.class, args);
  }
}
