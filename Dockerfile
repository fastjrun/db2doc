FROM openjdk:8-alpine

ADD db2doc.jar /opt
ADD db2doc.sh /opt

WORKDIR /opt

ENV JAVA_OPTS "-Xms256m -Xmx256m"

ENV INIT "always"

ENTRYPOINT sh /opt/db2doc.sh

